//
//  ViewController.h
//  CameraSample
//
//  Created by Mandeep Sharma on 11/01/17.
//  Copyright © 2017 ImpingeSolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate , UIAlertViewDelegate>


@end

