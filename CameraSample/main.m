//
//  main.m
//  CameraSample
//
//  Created by Mandeep Sharma on 11/01/17.
//  Copyright © 2017 ImpingeSolutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
